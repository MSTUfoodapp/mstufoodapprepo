var RecipeView = Parse.View.extend({
	className: 'recipe',
  template: _.template($('#recipeTMP').html()),	// I create the template function as a property of the view
// 	events: {
// 		'click .like': 'like',
// 		'click .unlike': 'unlike'
// 	},
	initialize: function() {

	},
	render: function() {

		this.$el.html(this.template(this.model.toJSON()));
		// this.updateLikeButton();
		// this.updateUsers();
		// this.colorize();

		return this;
	},
// 	colorize: function() {
// 		var backgroundColor = BookView.randomColor();
// 		var textColor = BookView.contrastColor(backgroundColor);
// 		this.$el.css({
// 			'background-color': backgroundColor.rgbText,
// 			'color': textColor.rgbText
// 		});
// 	},
// 	updateUsers: function() {
// 		var $container = $('<div>');

// 		_.each(this.model.get('likes'), function(user, index, list) {
// 			var $userLink = $('<a>');
// 			var userFullName = user.get('first') + ' ' + user.get('last');

// 			$userLink.attr('href', 'user.html?userID=' + user.id + '&first=' + user.get('first') + '&last=' + user.get('last'));
// 			$userLink.html(userFullName);

// 			$container.append($userLink);

// 			if (index < list.length - 1) {
// 				$container.append(', ');
// 			}
// 		});

// 		this.$('.users').html($container.html());
// 	},
// 	updateLikeButton: function() {
// 		// Is user in likes array? Find if it is...
// 		var user = _.find(this.model.get('likes'), function(user) {
// 			return user.id === Parse.User.current().id;
// 		});

// 		if (user) {
// 			this.$('.like').addClass('hidden');
// 			this.$('.unlike').removeClass('hidden');
// 		} else {
// 			this.$('.like').removeClass('hidden');
// 			this.$('.unlike').addClass('hidden');
// 		}

// 		var count = this.model.get('likes').length;
// 		this.$('.count').html(count);
// 	},
// 	like: function() {
// 		// addUnique() ensures that a user can only be added once
// 		this.model.addUnique('likes', Parse.User.current());
// 		this.model.save();
// 		this.render();
// 	},
// 	unlike: function() {
// 		this.model.remove('likes', Parse.User.current());
// 		this.model.save();
// 		this.render();
// 	}
// }, {	// If you want to create STATIC methods you can include a second optional object argument
// 	randomColor: function() {
// 		// rgb(255,255,255) etc.
// 		var red = Math.ceil(Math.random() * 255);
// 		var blue = Math.ceil(Math.random() * 255);
// 		var green = Math.ceil(Math.random() * 255);

// 		var rgbTEXT = 'rgb(' + red + ',' + green + ',' + blue + ')';

// 		var rgb = {
// 			red: red,
// 			green: green,
// 			blue: blue,
// 			rgbText: rgbTEXT
// 		};

// 		return rgb; // returns a 'color object'
// 	},
// 	// Adapted from StackOverflow http://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
// 	contrastColor: function(color) {
// 		var d = 0;
// 		var a = 1 - (0.299 * color.red + 0.587 * color.green + 0.114 * color.blue)/255;

// 		if (a < 0.5) {
// 			d = 0;
// 		} else {
// 			d = 255;
// 		}

// 		var rgb = {
// 			red: d,
// 			green: d,
// 			blue: d,
// 			rgbText: 'rgb(' + d + ',' + d + ',' + d + ')'
// 		};

// 		return rgb;
// 	}
});




// fancybox
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});





     




