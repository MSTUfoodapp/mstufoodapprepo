var RecipeDetailView = Parse.View.extend({
	className: 'recipe',
  template: _.template($('#detailRecipeTMP').html()),	// I create the template function as a property of the view
// 	events: {
// 		'click .like': 'like',
// 		'click .unlike': 'unlike'
// 	},
	initialize: function() {

	},
	render: function() {

		this.$el.html(this.template(this.model.toJSON()));
		// this.updateLikeButton();
		// this.updateUsers();
		// this.colorize();

		return this;
	},