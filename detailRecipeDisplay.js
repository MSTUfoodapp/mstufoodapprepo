var Recipe = Parse.Object.extend('Recipe', {
	defaults: {
		recipeTitle: 'Anonymous',
		cookingTime: 'No Title',
		allIngredients: '1970',
		cookingSteps:'None',
		// likes: []
	},
	initialize: function() {
		console.log('recipe initialized');
	},
	getAPAHTML: function() {
		return this.model('recipeTitle') + '(' + this.model.get('cookingTime') + '), <em>' + this.model.get('allIngredients') + '</em>;';
	}
});